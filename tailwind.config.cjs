/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      fontFamily: {
        'sans': ['inter', 'sans-serif'],
        'play': ['play', 'sans-serif']
      }
    },
  },
  plugins: [],
}
